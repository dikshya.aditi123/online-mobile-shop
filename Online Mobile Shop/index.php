<?php 

ob_start();
//include header.php file
include('header.php');


//include banner area
include('template/_bannerarea.php');


//include top selling area
include('template/_topselling.php');


//include on sale area
include('template/_onsale.php');


//include banner ads area
include('template/_bannerads.php');


//include new phones area
include('template/_newphones.php');


//include footer.php file
include('footer.php');
?>