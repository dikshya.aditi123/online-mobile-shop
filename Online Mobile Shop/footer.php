</main>
<!-- End main -->
<!-- Start footer -->
<footer id="footer" class="bg-dark text-white py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-12">
                <h4 class="font-rubik font-size-20">Online Mobile Shop</h4>
                <p class="font-size-14 font-rale text-white-50">Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem minus eos tempora dolor, magnam assumenda libero aliquid nisi, corrupti atque officia exercitationem eligendi ex, eaque earum distinctio quaerat nam cum.</p>
            </div>
            <div class="col-lg-4 col-12">
                <h4 class="font-rubik font-size-20">Newsletter</h4>
                <form>
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Email*">
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-primary">Subscribe</button>
                        </div>
                    </div>
                </form>

            </div>
            <div class="col-lg-2 col-12">
                <h4 class="font-rubik font-size-20">Quick Links</h4>
                <div class="d-flex flex-column flex-wrap">
                    <a href="index.php" class="font-rale font-size-14 text-white-50 pb-1">Home</a>
                    <a href="allproducts.php" class="font-rale font-size-14 text-white-50 pb-1">All Products</a>
                    <a href="comparison.php" class="font-rale font-size-14 text-white-50 pb-1">Comparison</a>
                    <a href="onsale.php" class="font-rale font-size-14 text-white-50 pb-1">On Sale</a>
                    <a href="contactus.php" class="font-rale font-size-14 text-white-50 pb-1">Contact Us</a>
                    <a href="#" class="font-rale font-size-14 text-white-50 pb-1">Notifications</a>
                </div>
            </div>
            <div class="col-lg-2 col-12">
                <h4 class="font-rubik font-size-20">Account</h4>
                <div class="d-flex flex-column flex-wrap">
                    <a href="#" class="font-rale font-size-14 text-white-50 pb-1">My Account</a>
                    <a href="#" class="font-rale font-size-14 text-white-50 pb-1">Order History</a>
                    <a href="cart.php" class="font-rale font-size-14 text-white-50 pb-1">WishList</a>
                    <a href="onsale.php" class="font-rale font-size-14 text-white-50 pb-1">On Sale</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->
<div class="copyright text-center bg-dark text-white py-2">
    <p>&copy;Copyrights @2021. All rights reserved by <a href="" class="color-secod">Online Mobile Shop</a> </p>
</div>



<!-- Bootstrap core JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

<!-- owl carousel js file -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<!-- isotope plugin js cdn -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js" integrity="sha512-Zq2BOxyhvnRFXu0+WE6ojpZLOU2jdnqbrM1hmVdGzyeCa1DgM3X5Q4A/Is9xA1IkbUeDd7755dNNI/PzSf2Pew==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<!-- custom js file -->
<script src="index.js"></script>


</body>

</html>