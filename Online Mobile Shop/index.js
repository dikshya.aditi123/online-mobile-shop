$(document).ready(function(){
    //banner owl carousel
    $("#banner-area .owl-carousel").owlCarousel({
        dots:true,
        items:1,
        loop:true,
        autoplayTimeout:2000, //2000 ms = 2 sec
        autoplayhoverpause:true

    });
    // Top Selling Owl Carousel
    $("#top-selling .owl-carousel").owlCarousel({
        loop:true,
        nav:true,
        dots:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    });

    // isotope 
    $(".grid").isotope({
        itemSelector:'.grid-item',
        layoutMode:'fitRows'       
    });
    
    // New Phones Owl Carousel
    $("#new-phones .owl-carousel").owlCarousel({
        loop:true,
        nav:false,
        dots:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    });

    //product quantity section
    let $qty_down = $(".qty .qty-down");
    let $qty_up = $(".qty .qty-up");
    let $deal_price = $("#deal-price");
    // let $input = $(".qty .qty-input");
    
    //click on qty up button
    $qty_up.click(function(e){

        let $input =$(`.qty-input[data-id='${$(this).data("id")}']`);
        let $price =$(`.product-price[data-id = '${$(this).data("id")}']`);
        
        //change price using ajax call
        $.ajax({url:'template/ajax.php', type:'POST',data:{itemid:$(this).data("id")},success:function(result){
            
            
            let obj = JSON.parse(result);

            let item_price = obj[0]['discounted_price'];
        
            if($input.val()>=1 && $input.val()<=9){
                $input.val(function(i,oldval){
                    return ++oldval;
                });
                //increase price
             $price.text(parseInt(item_price*$input.val()).toFixed(2));
            

             //subtotal price_show
             let subtotal = parseInt($deal_price.text())+parseInt(item_price);
             $deal_price.text(subtotal.toFixed(2));
             }
            }
            
        });//closing ajax request
        
    });

    //click on qty down button
    $qty_down.click(function(e){

        let $input =$(`.qty-input[data-id='${$(this).data("id")}']`);
        let $price =$(`.product-price[data-id = '${$(this).data("id")}']`);

        //change price using ajax call
        $.ajax({url:'template/ajax.php', type:'POST',data:{itemid:$(this).data("id")},success:function(result){
            
            
            let obj = JSON.parse(result);

            let item_price = obj[0]['discounted_price'];
        
            if($input.val()>1 && $input.val()<=10){
                $input.val(function(i,oldval){
                    return --oldval;
                });
                //increase price
             $price.text(parseInt(item_price*$input.val()).toFixed(2));
            

             //subtotal price_show
             let subtotal = parseInt($deal_price.text())-parseInt(item_price);
             $deal_price.text(subtotal.toFixed(2));
             }
            }
            
        });//closing ajax request
        
    });

    
});